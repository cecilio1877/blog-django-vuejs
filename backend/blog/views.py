from django.shortcuts import get_object_or_404, render
from rest_framework.views import APIView
from rest_framework.response import Response

from .models import Post
from .serializers import PostSerializer


class BlogListView(APIView):
    def get(self, request, *args, **kwargs):
        posts = Post.objects.all()[0:4] # manejador que esta en models, me deja ver solo los 4 primeros publicados
        serializer = PostSerializer(posts, many=True) # queryset se muchos posts
        return Response(serializer.data)


class PostDetailView(APIView):
    def get(self, request, post_slug,  *args, **kwargs):
        post = get_object_or_404(Post, slug=post_slug) # se hace referencia al post especifico que quiero o me da un 404
        serializer = PostSerializer(post)

        return Response(serializer.data)
