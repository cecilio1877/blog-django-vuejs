from distutils.command.upload import upload
from msilib.schema import Class
from django.db import models
from django.utils import timezone
from django.contrib.auth.models import User

# directorio de las imagenes
def user_directory_path(instance, filename):
    return 'blog/{0}/{1}'.format(instance.title, filename)


    
class Post(models.Model):
    # manejador que se usa en las vistas para ver solo los publicados
    class PostObjects(models.Model):
        def get_queryset(self):
            return super().get_queryset() .filter(status='published')

# optiones del estado del post
    options = (
        ('draft', 'Draft'), 
        ('published', 'Published')
    )

    title = models.CharField(max_length=250)
    thumbnail = models.ImageField(upload_to=user_directory_path, blank=True, null=True)
    excerpt = models.TextField(null=True)
    content = models.TextField()
    slug = models.SlugField(max_length=250, unique_for_date='published', null=False, unique=True)
    published = models.DateTimeField(default=timezone.now)
    author = models.ForeignKey(User, on_delete=models.CASCADE, related_name='post_user')
    
    status = models.CharField(max_length=10, choices=options, default='draft')
    
    objects = models.Manager() # default manager
    postobjects = PostObjects() # custon manager

    class Meta:
        ordering = ('-published',)

    def __str__(self):
        return self.title


