# Blog django vuejs

Blog creado con django rest para el backend

Vuejs para el frontend

## run locally
- **Create a virtual env:**

  (env is the name of virtualenv, maybe any)

  `python -m venv env`

- **Activate the virtual environment:**

  Windows:

  - `env\Scripts\activate.bat`

  Linux:

  - `source env/bin/activate`

  Mac:

  - `. env/bin/activate`

- **Install requirements file**

  `pip install -r requirements.txt`

- **Run local server**

  default port:8000

  `python manage.py runserver`